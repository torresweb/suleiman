#!/usr/bin/env python3
import random
import heapq
import pandas as pd
import crcmod.predefined
import redis
import pickle
import dpkt
import socket
# from struct import pack, unpack
import random
from multiprocessing import Process
from threading import Thread
from math import ceil
import time

random.seed(0)

# see here: http://crcmod.sourceforge.net/crcmod.predefined.html
func_list = ['crc-32', 'crc-32c', 'crc-32d', 'crc-32q', 'xfer', 'posix', 'jamcrc', 'crc-32-bzip2', 'crc-32-mpeg', 'crc-64']
crc_funcs = [crcmod.predefined.mkCrcFun(x) for x in func_list]

class Register(object):
    def __init__(self, size):
        super(Register, self).__init__()
        self.size = size
        self.register = list()
        for position in range (0, self.size):
            self.register.append(0)

    def clear(self):
        for position in range (0, self.size):
            self.register[position] = 0

    def read(self, position):
        if position >= self.size:
            raise NameError('Out of space')
        return self.register[position]

    def write(self, position, val):
        if position >= self.size:
            raise NameError('Out of space')
        self.register[position] = val

class SketchDRegisters(object):
    def __init__(self, tagname, filename, repeats, deep, name, m, timeout, duration, size=1, samples=0, sampling_rate=1, verbose=False, topkeys=[]):
        assert deep >= 2, 'Deep must be >= 2'
        assert m > 0, 'Entries must be > 0'
        assert timeout > 0, 'Flow timeout must be > 0'
        assert duration > 0, 'Flow duration must be > 0'
        assert size >= 1, 'Flow size must be >= 1'
        assert samples >= 0, 'Flow samples must be >= 0'
        assert sampling_rate >= 1, 'Sampling Rate must be >= 1'
        self.type = 'ske'
        self.filename = filename
        self.tagname = tagname
        self.repeats = repeats
        self.deep = deep
        self.name = name
        self.m = m # Agora aqui eh a memoria
        # A quantidade de entrada vai ser o valor de m dividido pelo numero de sketchs.
        self.entries = int((self.m / 12) / self.deep) # aqui eh o numero de entradas por tabela (profundidade)
        self.timeout = timeout
        self.duration = duration
        self.size = size # Only used if samples = 0
        self.samples = samples
        self.sampling_rate = sampling_rate
        self.topkeys = topkeys
        self.verbose = False
        # self.regTrack = dict()
        self.regPackets = dict()
        self.regBytes = dict()
        self.regFirst = dict()
        self.regLast = dict()
        self.isElephant = dict()
        for d in range(0, self.deep):
            # Em Track iremos manter o valor do flowid para poder identificar os HH
            # self.regTrack[d] = Register(self.entries)
            self.regPackets[d] = Register(self.entries)
            self.regBytes[d] = Register(self.entries)
            self.regFirst[d] = Register(self.entries)
            self.regLast[d] = Register(self.entries)
            self.isElephant[d] = Register(self.entries)
        # Para controle:
        self.collision = 0
        self.notifications = 0
        self.true_notifications = 0
        self.expired = 0
        self.uniq_notify = dict()
        self.managed_bytes = 0
        self.true_bytes = 0
        self.fake_notifications_uniq = dict()
        # self.notified = dict() # only for control managed_bytes
        self.sampled_packets = 0
        self.total_rw = 0

    def clear(self):
        for d in range(0, self.deep):
            # Em Track iremos manter o valor do flowid para poder identificar os HH
            # self.regTrack[d] = Register(self.entries)
            # import pdb; pdb.set_trace()
            self.regPackets[d].clear()
            self.regBytes[d].clear()
            self.regFirst[d].clear()
            self.regLast[d].clear()
            self.isElephant[d].clear()
        # Para controle:
        self.collision = 0
        self.notifications = 0
        self.true_notifications = 0
        self.expired = 0
        self.uniq_notify.clear()
        self.managed_bytes = 0
        self.true_bytes = 0
        self.fake_notifications_uniq.clear()
        # self.notified = dict() # only for control managed_bytes
        self.sampled_packets = 0
        self.total_rw = 0

    def inc(self, PktId):
        try:
            if self.uniq_notify[PktId.key]:
                self.managed_bytes += PktId.bytes
                if PktId.key in self.topkeys:
                    self.true_bytes += PktId.bytes
        except:
            pass


        rnd = 0
        if self.sampling_rate > 1:
            rnd = random.randint(0, self.sampling_rate-1)

        if (rnd != 0):
            return

        self.sampled_packets += 1
        reg_pos = compute_hashes(PktId.key.encode(), self.entries, self.deep)

        val_last_list = list()
        for d in range(0, self.deep):
            # Tracker do fluxo
            # self.regTrack[d].write(reg_pos[d], PktId.key)
            # Le todos os last para verificar se ocorreu timeout
            val_last_list.append(self.regLast[d].read(reg_pos[d]))
            self.total_rw += 1

        val_last = max(val_last_list)
        cur_timeout = PktId.tstamp - val_last

        val_first_list = list()
        val_packets_list = list()
        val_bytes_list = list()
        # Se ocorreu timeout fazemos um reset nos sketchs
        is_timeout = False
        if (cur_timeout > self.timeout):
            is_timeout = True
            val_bytes = PktId.bytes
            val_packets = 1
            val_first =  PktId.tstamp
            val_first_list.append(val_first)
            for d in range(0, self.deep):
                self.regFirst[d].write(reg_pos[d], val_first)
                self.isElephant[d].write(reg_pos[d], 0)
                self.total_rw += 2

        is_elephant_list = list()
        for d in range(0, self.deep):
            self.regLast[d].write(reg_pos[d], PktId.tstamp);
            if not is_timeout:
                val_packets = self.regPackets[d].read(reg_pos[d]) + 1
                val_bytes = self.regBytes[d].read(reg_pos[d]) + PktId.bytes
                self.total_rw += 2
            # Precisa guardar para verificar se alcancou threshould
            val_packets_list.append(val_packets)
            val_bytes_list.append(val_bytes)
            self.regPackets[d].write(reg_pos[d], val_packets)
            self.regBytes[d].write(reg_pos[d], val_bytes)
            self.total_rw += 2
            is_elephant_list.append(self.isElephant[d].read(reg_pos[d]))
            self.total_rw += 1

        # Se o min for 1, entao muito provavel que tenha sido classificado como elefante em todos os sketchs
        val_elephant = min(is_elephant_list)
        # Se for zero em algum sketch, precisa ser ativado notificacao.
        if (val_elephant == 0 and not is_timeout):
            for d in range(0, self.deep):
                val_first_list.append(self.regFirst[d].read(reg_pos[d]))
            cur_duration = PktId.tstamp - max(val_first_list)
            if self.samples == 0:
                cond = (cur_duration > self.duration and min(val_bytes_list) > self.size)
            else:
                cond = (cur_duration > self.duration and min(val_packets_list) > self.samples)
            if cond:
                if self.verbose:
                    print ('%s: ID: %s Bytes: %s Packets: %s Duration: %s' % (self.name, PktId.key, min(val_bytes_list), min(val_packets_list), cur_duration))
                self.notifications += 1
                try:
                    self.uniq_notify[PktId.key]
                except:
                    self.managed_bytes += PktId.bytes
                    if PktId.key in self.topkeys:
                        self.true_bytes += PktId.bytes
                self.uniq_notify[PktId.key] = True
                if PktId.key in self.topkeys:
                    # self.true_bytes += PktId.bytes
                    self.true_notifications += 1
                else:
                    self.fake_notifications_uniq[PktId.key] = True

                # self.notified[PktId.key] = True
                for d in range(0, self.deep):
                    # Comentado para ver comportamento sem bloomfilter
                    self.isElephant[d].write(reg_pos[d], 1)
                    self.total_rw += 1

class SpaceSavingHeap(object):
    def __init__(self, tagname, filename, repeats, name, m, timeout, duration, size=1, samples=0, sampling_rate=1, victim_type='tstamp', reset_type='reset', verbose=False, topkeys=[]):
        assert m > 0, 'Entries must be > 0'
        assert timeout > 0, 'Flow timeout must be > 0'
        assert duration > 0, 'Flow duration must be > 0'
        assert size >= 1, 'Flow size must be >= 1'
        assert samples >= 0, 'Flow samples must be >= 0'
        assert sampling_rate >= 1, 'Sampling Rate must be >= 1'
        assert victim_type in ['tstamp', 'bytes', 'packets'], 'Victim type error'
        assert reset_type in ['reset', 'keep'], 'Reset type error'
        self.type = 'ssh'
        self.filename = filename
        self.tagname = tagname
        self.repeats = repeats
        self.name = name
        self.m = m
        self.timeout = timeout
        self.duration = duration
        self.size = size # Only used if samples = 0
        self.samples = samples
        self.sampling_rate = sampling_rate
        self.topkeys = topkeys
        self.victim_type = victim_type
        self.reset_type = reset_type
        self.entries = int(self.m / 20) # Quantidade e entrada (considerando o flowid de 12B)
        self.k = self.entries
        self.verbose = verbose
        self.bytes = dict()
        self.last = dict()
        self.first = dict()
        self.notified = dict()
        self.packets = dict()
        self.queue = []
        # Para controle:
        self.outofspace = 0
        self.notifications = 0
        self.true_notifications = 0
        self.expired = 0
        self.uniq_notify = dict()
        self.managed_bytes = 0
        self.true_bytes = 0
        self.fake_notifications_uniq = dict()
        self.sampled_packets = 0
        self.total_rw = 0
        self.heap_cost = 0

    def clear(self):
        self.k = self.entries
        self.bytes.clear()
        self.last.clear()
        self.first.clear()
        self.notified.clear()
        self.packets.clear()
        self.queue.clear()
        # Para controle:
        self.outofspace = 0
        self.notifications = 0
        self.true_notifications = 0
        self.expired = 0
        self.uniq_notify.clear()
        self.managed_bytes = 0
        self.true_bytes = 0
        self.fake_notifications_uniq.clear()
        self.sampled_packets = 0
        self.total_rw = 0
        self.heap_cost = 0

    def inc(self, PktId):
        try:
            if self.uniq_notify[PktId.key]:
                self.managed_bytes += PktId.bytes
                if PktId.key in self.topkeys:
                    self.true_bytes += PktId.bytes
        except:
            pass

        rnd = 0
        if self.sampling_rate > 1:
            rnd = random.randint(0, self.sampling_rate-1)

        if (rnd != 0):
            return

        self.sampled_packets += 1

        try:
            cur_timeout = PktId.tstamp - self.last[PktId.key]
            watched = True
        except:
            watched = False

        # Sempre atualiza o last
        self.last[PktId.key] = PktId.tstamp
        self.total_rw += 2 # considering the read in try:
        if watched:
            # Verificar se ocorreu um timeout, se ocorreu, resetamos.
            # Um reset permite que um fluxo seja renotificado, mas nao ajuda com colisoes.
            if (cur_timeout > self.timeout):
                self.first[PktId.key] = PktId.tstamp
                self.bytes[PktId.key] = PktId.bytes
                self.packets[PktId.key] = 1
                self.expired += 1
                # Reset controller
                self.notified[PktId.key] = False
                self.total_rw += 3 # considering first, bytes xor packets, notified
            else:
                self.bytes[PktId.key] += PktId.bytes
                self.packets[PktId.key] += 1
                self.total_rw += 1
        else:
            # make room for key
            if (self.k == 0):
                # Quando nao ha mais espaco, vitimiza quem esta a mais tempo sem atualizar, o menor last tstamp
                # Opcoes. Usar o bytes e first da vitima, ou resetar.
                while True:
                    self.heap_cost += 1
                    if self.victim_type == 'bytes':
                        bytes, tstamp, packets, ckey = self.pop()
                        val_test = bytes
                        cur_val = self.bytes[ckey]
                    elif self.victim_type == 'packets':
                        packets, tstamp, bytes, ckey = self.pop()
                        val_test = packets
                        cur_val = self.packets[ckey]
                    # Quando nao ha mais espaco, vitimiza quem esta a mais tempo sem atualizar, o menor last tstamp
                    elif self.victim_type == 'tstamp':
                        tstamp, bytes, packets, ckey = self.pop()
                        val_test = tstamp
                        cur_val = self.last[ckey]
                    if cur_val == val_test:
                        del self.bytes[ckey]
                        del self.first[ckey]
                        del self.last[ckey]
                        del self.packets[ckey]
                        del self.notified[ckey]
                        self.outofspace += 1
                        self.total_rw += 5 # for remove entry bytes xor packets, first, last, notified
                        break
                    else:
                        # Atualiza o valor de last na heap.
                        if self.victim_type == 'bytes':
                            self.push(self.bytes[ckey], self.last[ckey], self.packets[ckey], ckey)
                        elif self.victim_type == 'packets':
                            self.push(self.packets[ckey], self.last[ckey], self.bytes[ckey], ckey)
                        elif self.victim_type == 'tstamp':
                            self.push(self.last[ckey], self.bytes[ckey], self.packets[ckey], ckey)
                    self.total_rw += 1 # for push
                    self.total_rw += 2 # for pop and read last
            else:
                self.k -= 1
                bytes = PktId.bytes
                tstamp = PktId.tstamp
                packets = 1

            # watch key
            self.notified[PktId.key] = False
            if self.reset_type == 'keep':
                # Assign the min
                self.bytes[PktId.key] = bytes
                self.packets[PktId.key] = packets
                self.first[PktId.key] = tstamp
                if self.victim_type == 'bytes':
                    self.push(bytes, tstamp, packets, PktId.key)
                elif self.victim_type == 'packets':
                    self.push(packets, tstamp, bytes, PktId.key)
                elif self.victim_type == 'tstamp':
                    self.push(tstamp, bytes, packets, PktId.key)
            elif self.reset_type == 'reset':
                # Reset counters
                self.bytes[PktId.key] = PktId.bytes
                self.packets[PktId.key] = 1
                self.first[PktId.key] = PktId.tstamp # Current
                if self.victim_type == 'bytes':
                    self.push(PktId.bytes, PktId.tstamp, 1, PktId.key)
                elif self.victim_type == 'packets':
                    self.push(1, PktId.tstamp, PktId.bytes, PktId.key)
                elif self.victim_type == 'tstamp':
                    self.push(PktId.tstamp, PktId.bytes, 1, PktId.key)
            self.total_rw += 4

        # Se ainda nao foi notificado precisa verificar se alcancou os limiares.
        if not self.notified[PktId.key]:
            cur_duration = PktId.tstamp - self.first[PktId.key]
            if self.samples == 0:
                cond = (cur_duration > self.duration and self.bytes[PktId.key] > self.size)
            else:
                cond = (cur_duration > self.duration and self.packets[PktId.key] > self.samples)
            if cond:
                if self.verbose:
                    print ('%s: ID: %s Bytes: %s Packets: %s Duration: %s' % (self.name, PktId.key, self.bytes[PktId.key], self.packets[PktId.key], cur_duration))
                self.notified[PktId.key] = True
                self.notifications += 1
                try:
                    self.uniq_notify[PktId.key]
                except:
                    self.managed_bytes += PktId.bytes
                    if PktId.key in self.topkeys:
                        self.true_bytes += PktId.bytes
                self.uniq_notify[PktId.key] = True
                if PktId.key in self.topkeys:
                    # self.true_bytes += PktId.bytes
                    self.true_notifications += 1
                else:
                    self.fake_notifications_uniq[PktId.key] = True
                self.total_rw += 1 # for w notified
            self.total_rw += 2 # for first and bytes xor packets
        self.total_rw += 1 # for r notified


    def push(self, val1, val2, val3, key):
        heapq.heappush(self.queue, (val1, val2, val3, key))

    def pop(self):
        return heapq.heappop(self.queue)

    def topN(self, N):
        most_common = Counter(self.bytes).most_common(N)
        for km, vm in most_common:
            print ('%s: %i' % (km, vm))

class PktIdInfo:
    def __init__(self, key, bytes, tstamp):
        self.key = key
        self.bytes = bytes
        self.tstamp = tstamp

class RunFile(object):
    def __init__(self, setup, single=False):
        self.setup = setup
        if single:
            self.thread = Thread(target=self.run, args=())
        else:
            self.thread = Process(target=self.run, args=())
        self.thread.start()

    def run(self):

        config = self.setup

        r = redis.Redis(host='192.168.200.1', port=6379, db=0)
        tagnameWorking = config.tagname + '_Working'
        tagnameFinished = config.tagname + '_Finished'

        try:
            r.lpush(tagnameWorking, config.obj_name)
        except:
            r = redis.Redis(host='192.168.200.1', port=6379, db=0)
            r.lpush(tagnameWorking, config.obj_name)

        try:
            topkeys = pd.read_csv(config.topkeys, names=['flowkey']).to_dict(orient='list')['flowkey']
        except:
            print ('no topkeys file')
            topkeys = []

        if config.type == 'ske':
            obj = SketchDRegisters(config.tagname, config.filename, config.repeats, config.deep, config.obj_name, config.memory, config.timeout, config.duration, config.size, config.samples, config.srate, config.verbose, topkeys)
        elif config.type == 'ssh':
            obj = SpaceSavingHeap(config.tagname, config.filename, config.repeats, config.obj_name, config.memory, config.timeout, config.duration, config.size, config.samples, config.srate, config.victim_type, config.reset_type, config.verbose, topkeys)
        else:
            return

        skipL2 = False
        if config.filename in ['caidaDirA.pcap', 'caidaDirB.pcap']:
            skipL2 = True

        setup_res = {}
        obj_id = 0
        for rpt in range(obj.repeats):
            total_l2packets = 0
            total_l2error = 0
            total_l3bytes = 0
            total_l3error = 0
            total_noip_packets = 0
            total_l3packets = 0
            total_l4packets = 0
            total_l4bytes = 0
            distinct_l4flows = dict()
            print ('setup name %s rpt %s' % (obj.name, rpt))
            for ts, pkt in dpkt.pcap.Reader(open(obj.filename,'rb')):
                if not skipL2:
                    total_l2packets += 1
                    try:
                        eth = dpkt.ethernet.Ethernet(pkt)
                    except:
                        total_l2error += 1
                        continue
                    # if ((eth.type != dpkt.ethernet.ETH_TYPE_IP)):
                    if ((eth.type != dpkt.ethernet.ETH_TYPE_IP) and (eth.type != dpkt.ethernet.ETH_TYPE_IP6)):
                        total_noip_packets += 1
                        continue
                    ip = eth.data
                else:
                    try:
                        ip = dpkt.ip.IP(pkt)
                    except:
                        total_l3error += 1
                        continue

                total_l3packets += 1
                sizeerror = False
                try:
                    psize = ip.len
                except:
                    sizeerror = True

                if sizeerror:
                    try:
                        psize = ip.plen
                    except:
                        continue
                # try:
                #     psize = ip.len
                # except:
                #     psize = ip.plen

                total_l3bytes += psize

                if ((ip.p == dpkt.ip.IP_PROTO_TCP) or (ip.p == dpkt.ip.IP_PROTO_UDP)):
                    total_l4packets += 1
                    total_l4bytes += psize
                    l4 = ip.data
                    try:
                        flowid = 'p'  + str(ip.p) + ' ' + inet_to_str(ip.src) + ':' + str(l4.sport) + ' > ' + inet_to_str(ip.dst) + ':' + str(l4.dport)
                    except:
                        continue

                    distinct_l4flows[flowid] = True

                    f = PktIdInfo(flowid, psize, ts)
                    obj.inc(f)

            if obj.type == 'ske':
                setup_res[obj_id] = [obj.filename, obj.repeats, obj.type, obj.deep, obj.m, obj.entries, obj.timeout, obj.duration, obj.size, obj.samples, obj.sampling_rate, obj.name, obj.collision, float('NaN'), obj.notifications, obj.true_notifications, obj.expired, len(obj.uniq_notify), obj.managed_bytes, obj.sampled_packets, obj.total_rw, float('NaN'), float('NaN'), float('NaN'), obj.true_bytes, len(obj.fake_notifications_uniq)]
            elif obj.type == 'ssh':
                setup_res[obj_id] = [obj.filename, obj.repeats, obj.type, float('NaN'), obj.m, obj.entries, obj.timeout, obj.duration, obj.size, obj.samples, obj.sampling_rate, obj.name, float('NaN'), obj.outofspace, obj.notifications, obj.true_notifications, obj.expired, len(obj.uniq_notify), obj.managed_bytes, obj.sampled_packets, obj.total_rw, obj.heap_cost, obj.victim_type, obj.reset_type, obj.true_bytes, len(obj.fake_notifications_uniq)]
            if not obj.name == 'sshtop':
                obj.clear()
            obj_id += 1

        if obj.name == 'sshtop':
            # import pdb; pdb.set_trace()
            topflows = pd.DataFrame.from_dict(obj.uniq_notify, orient='index').reset_index()
            # import pdb; pdb.set_trace()
            topflows['index'].to_csv('topflows_' + str(obj.filename) + '.csv', header=False, index=False)
            # print (obj.uniq_notify)
            print( 'notifications %s' % (obj.notifications))
            print( 'uniq_notify %s' % (len(obj.uniq_notify)))
            print( 'managed_bytes %s' % (obj.managed_bytes))
            print( 'total_rw %s' % (obj.total_rw))

        # Save a file with all repeats
        # Todo: fix here to include memory and entries
        setups_col = ('filename', 'repeat', 'type', 'deep', 'memory', 'entries' ,'timeout', 'duration', 'fsize', 'samples', 'sampling_rate', 'name', 'collision', 'outofspace', 'notifications', 'true_notifications', 'expired', 'uniq_notify', 'managed_bytes', 'sampled_packets', 'total_rw', 'heap_cost', 'victim_type', 'reset_type', 'true_bytes', 'fake_notifications_uniq')
        res_df = pd.DataFrame.from_dict(setup_res, orient='index', columns=setups_col)
        csv_header = False
        res_df.to_csv('results/' + str(obj.tagname) + 'results_' + str(obj.name) + '.csv', header=csv_header, index=False)

        # Save a file per run
        runs_col = ('total_l2packets', 'total_l2error', 'total_l3bytes', 'total_noip_packets', 'total_l3packets', 'total_l4packets', 'total_l4bytes', 'distinct_l4flows')
        runs_res = {}
        runs_res[0] = [total_l2packets, total_l2error, total_l3bytes, total_noip_packets, total_l3packets, total_l4packets, total_l4bytes, len(distinct_l4flows)]

        res_df = pd.DataFrame.from_dict(runs_res, orient='index', columns=runs_col)
        csv_header = True
        res_df.to_csv('results/' + str(obj.tagname) + 'run_' + str(obj.name) + '.csv', header=csv_header, index=False)

        try:
            r.lpush(tagnameFinished, config.obj_name)
        except:
            r = redis.Redis(host='192.168.200.1', port=6379, db=0)
            r.lpush(tagnameFinished, config.obj_name)



    def run_old(self):
        obj = self.setup
        setup_res = {}
        obj_id = 0
        for rpt in range(obj.repeats):
            total_l2packets = 0
            total_l2error = 0
            total_l3bytes = 0
            total_l3error = 0
            total_noip_packets = 0
            total_l3packets = 0
            total_l4packets = 0
            total_l4bytes = 0
            distinct_l4flows = dict()
            print ('setup name %s rpt %s' % (obj.name, rpt))
            for ts, pkt in dpkt.pcap.Reader(open(obj.filename,'rb')):
                if not self.skipL2:
                    total_l2packets += 1
                    try:
                        eth = dpkt.ethernet.Ethernet(pkt)
                    except:
                        total_l2error += 1
                        continue
                    # if ((eth.type != dpkt.ethernet.ETH_TYPE_IP)):
                    if ((eth.type != dpkt.ethernet.ETH_TYPE_IP) and (eth.type != dpkt.ethernet.ETH_TYPE_IP6)):
                        total_noip_packets += 1
                        continue
                    ip = eth.data
                else:
                    try:
                        ip = dpkt.ip.IP(pkt)
                    except:
                        total_l3error += 1
                        continue

                total_l3packets += 1
                sizeerror = False
                try:
                    psize = ip.len
                except:
                    sizeerror = True

                if sizeerror:
                    try:
                        psize = ip.plen
                    except:
                        continue
                # try:
                #     psize = ip.len
                # except:
                #     psize = ip.plen

                total_l3bytes += psize

                if ((ip.p == dpkt.ip.IP_PROTO_TCP) or (ip.p == dpkt.ip.IP_PROTO_UDP)):
                    total_l4packets += 1
                    total_l4bytes += psize
                    l4 = ip.data
                    try:
                        flowid = 'p'  + str(ip.p) + ' ' + inet_to_str(ip.src) + ':' + str(l4.sport) + ' > ' + inet_to_str(ip.dst) + ':' + str(l4.dport)
                    except:
                        continue

                    distinct_l4flows[flowid] = True

                    f = PktIdInfo(flowid, psize, ts)
                    # for obj in objs:
                    obj.inc(f)

            if obj.type == 'ske':
                setup_res[obj_id] = [obj.filename, obj.repeats, obj.type, obj.deep, obj.m, obj.timeout, obj.duration, obj.size, obj.samples, obj.sampling_rate, obj.name, obj.collision, float('NaN'), obj.notifications, obj.true_notifications, obj.expired, len(obj.uniq_notify), obj.managed_bytes, obj.sampled_packets, obj.total_rw, float('NaN'), float('NaN'), float('NaN'), obj.true_bytes, len(obj.fake_notifications_uniq)]
            elif obj.type == 'ssh':
                setup_res[obj_id] = [obj.filename, obj.repeats, obj.type, float('NaN'), obj.m, obj.timeout, obj.duration, obj.size, obj.samples, obj.sampling_rate, obj.name, float('NaN'), obj.outofspace, obj.notifications, obj.true_notifications, obj.expired, len(obj.uniq_notify), obj.managed_bytes, obj.sampled_packets, obj.total_rw, obj.heap_cost, obj.victim_type, obj.reset_type, obj.true_bytes, len(obj.fake_notifications_uniq)]
            if not obj.name == 'sshtop':
                obj.clear()
            # obj.clear()
            obj_id += 1

        # Save a file with all repeats
        setups_col = ('filename', 'repeat', 'type', 'deep', 'entries', 'timeout', 'duration', 'fsize', 'samples', 'sampling_rate', 'name', 'collision', 'outofspace', 'notifications', 'true_notifications', 'expired', 'uniq_notify', 'managed_bytes', 'sampled_packets', 'total_rw', 'heap_cost', 'victim_type', 'reset_type', 'true_bytes', 'fake_notifications_uniq')
        res_df = pd.DataFrame.from_dict(setup_res, orient='index', columns=setups_col)
        csv_header = False
        res_df.to_csv('results/' + str(obj.tagname) + 'results_' + str(obj.name) + '.csv', header=csv_header, index=False)

        # Save a file per run
        runs_col = ('total_l2packets', 'total_l2error', 'total_l3bytes', 'total_noip_packets', 'total_l3packets', 'total_l4packets', 'total_l4bytes', 'distinct_l4flows')
        runs_res = {}
        runs_res[0] = [total_l2packets, total_l2error, total_l3bytes, total_noip_packets, total_l3packets, total_l4packets, total_l4bytes, len(distinct_l4flows)]

        res_df = pd.DataFrame.from_dict(runs_res, orient='index', columns=runs_col)
        csv_header = True
        res_df.to_csv('results/' + str(obj.tagname) + 'run_' + str(obj.name) + '.csv', header=csv_header, index=False)

class ComputeBytes(object):
    def __init__(self, filename, topflow, skipL2=False):
        self.filename = filename
        self.topflow = topflow
        self.skipL2 = skipL2
        self.thread = Thread(target=self.run, args=())
        self.thread.start()

    def run(self):
        try:
            topkeys = pd.read_csv(self.topflow, names=['flowkey']).to_dict(orient='list')['flowkey']
        except:
            print ('no topkeys file')
            return

        setup_res = {}
        total_l2packets = 0
        total_l2error = 0
        total_l3bytes = 0
        total_l3error = 0
        total_noip_packets = 0
        total_l3packets = 0
        total_l4packets = 0
        total_l4bytes = 0
        managed_bytes = 0
        distinct_l4flows = dict()
        for ts, pkt in dpkt.pcap.Reader(open(self.filename,'rb')):
            if not self.skipL2:
                total_l2packets += 1
                try:
                    eth = dpkt.ethernet.Ethernet(pkt)
                except:
                    total_l2error += 1
                    continue
                # if ((eth.type != dpkt.ethernet.ETH_TYPE_IP)):
                if ((eth.type != dpkt.ethernet.ETH_TYPE_IP) and (eth.type != dpkt.ethernet.ETH_TYPE_IP6)):
                    total_noip_packets += 1
                    continue
                ip = eth.data
            else:
                try:
                    ip = dpkt.ip.IP(pkt)
                except:
                    total_l3error += 1
                    continue

            total_l3packets += 1
            sizeerror = False
            try:
                psize = ip.len
            except:
                sizeerror = True

            if sizeerror:
                try:
                    psize = ip.plen
                except:
                    continue

            total_l3bytes += psize

            if ((ip.p == dpkt.ip.IP_PROTO_TCP) or (ip.p == dpkt.ip.IP_PROTO_UDP)):
                total_l4packets += 1
                total_l4bytes += psize
                l4 = ip.data
                try:
                    flowid = 'p'  + str(ip.p) + ' ' + inet_to_str(ip.src) + ':' + str(l4.sport) + ' > ' + inet_to_str(ip.dst) + ':' + str(l4.dport)
                except:
                    continue

                if flowid in topkeys:
                    managed_bytes += psize

        print('managed_bytes %d' % (managed_bytes))

class SimulateFile(object):
    def __init__(self, numcores, tagname):
        super(SimulateFile, self).__init__()
        self.numcores = numcores
        self.tagname = tagname

    def consume(self):
        r = redis.Redis(host='192.168.200.1', port=6379, db=0)
        list_process = list()
        USED_CORES = 0
        while True:
            TODO = r.llen(self.tagname)
            if (USED_CORES < self.numcores) and TODO > 0:
                print ('TODO %s, USED_CORES %s' % (TODO, USED_CORES))
                try:
                    obj = pickle.loads(r.rpop(self.tagname))
                except:
                    continue
                batch = RunFile(obj, False)
                list_process.append(batch.thread)
                USED_CORES += 1
            else:
                if USED_CORES >= self.numcores :
                    print ('.', end='', flush=True)
                    updated_process = list()
                    for proc in list_process:
                        if not proc.is_alive():
                            USED_CORES -= 1
                        else:
                            updated_process.append(proc)
                    list_process = updated_process
                else:
                    print (':', end='', flush=True)
                time.sleep(15)

    def create_topflows(self, filename):
        obj_name = 'sshtop'
        entry =  60000000
        timeout = 10
        duration = 10
        size = 10000000

        victim_type = 'bytes'
        reset_type = 'keep'
        verbose = False
        topkeys = []
        # obj = SpaceSavingHeap(self.tagname, filename, 1, obj_name, entry, timeout, duration, size, 0, 1, victim_type, reset_type, verbose, topkeys)
        obj = ConfigProducer('ssh', self.tagname, filename, 1, None, obj_name, entry, timeout, duration, size, 0, 1, victim_type, reset_type, verbose, topkeys)
        batch = RunFile(obj, True)
        batch.thread.join()

class ConfigProducer(object):
    """docstring for ConfigProducer."""

    def __init__(self, type, tagname, filename, repeats, deep, obj_name, memory, timeout, duration, size, samples, srate, victim_type, reset_type, verbose, topkeys):
        self.type = type
        self.tagname = tagname
        self.filename = filename
        self.repeats = repeats
        self.deep = deep
        self.obj_name = obj_name
        self.memory = memory
        self.timeout = timeout
        self.duration = duration
        self.size = size
        self.samples = samples
        self.srate = srate
        self.victim_type = victim_type
        self.reset_type = reset_type
        self.verbose = verbose
        self.topkeys = topkeys

def compute_hashes(flowid, entries, deep=2):
    assert deep <= len(func_list), 'Too deep for compute hashes, increase the function list'
    hash_list = [(crc_funcs[d](flowid) % entries) for d in range(0, deep)]
    return hash_list

def inet_to_str(inet):
    # First try ipv4 and then ipv6
    try:
        return socket.inet_ntop(socket.AF_INET, inet)
    except ValueError:
        return socket.inet_ntop(socket.AF_INET6, inet)

if __name__ == '__main__':
    print ('nothing to do here')
