#!/usr/bin/env python3
# to be run in each cluster node

from suleiman_lib import *

if __name__ == '__main__':

    NUMCORES = 8
    tagname = 'FINAL'

    sim = SimulateFile(NUMCORES, tagname)
    sim.consume()
