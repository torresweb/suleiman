/* -*- P4_16 -*- */

#include <core.p4>
#include <v1model.p4>

const bit<16> TYPE_IPV4 = 0x800;
const bit<8>  TYPE_TCP  = 6;

#define FLOW_REGISTERS_ENTRIES 4096

#define FLOW_SIZE_THRESHOLD 10485760
// in microseconds: 4000000 = 4s
#define FLOW_DURATION_THRESHOLD 10000000
#define FLOW_TIMEOUT_THRESHOLD 10000000

/*************************************************************************
*********************** H E A D E R S  ***********************************
*************************************************************************/

typedef bit<9>  egressSpec_t;
typedef bit<48> macAddr_t;
typedef bit<32> ip4Addr_t;

header ethernet_t {
    macAddr_t dstAddr;
    macAddr_t srcAddr;
    bit<16>   etherType;
}

header ipv4_t {
    bit<4>    version;
    bit<4>    ihl;
    bit<6>    diffserv;
    bit<2>    ecn;
    bit<16>   totalLen;
    bit<16>   identification;
    bit<3>    flags;
    bit<13>   fragOffset;
    bit<8>    ttl;
    bit<8>    protocol;
    bit<16>   hdrChecksum;
    ip4Addr_t srcAddr;
    ip4Addr_t dstAddr;
}

header tcp_t{
    bit<16> srcPort;
    bit<16> dstPort;
    bit<32> seqNo;
    bit<32> ackNo;
    bit<4>  dataOffset;
    bit<4>  res;
    bit<1>  cwr;
    bit<1>  ece;
    bit<1>  urg;
    bit<1>  ack;
    bit<1>  psh;
    bit<1>  rst;
    bit<1>  syn;
    bit<1>  fin;
    bit<16> window;
    bit<16> checksum;
    bit<16> urgentPtr;
}

struct tuple5_t{
    ip4Addr_t srcAddr;
    ip4Addr_t dstAddr;
    bit<8>    protocol;
    bit<16> srcPort;
    bit<16> dstPort;
}

struct metadata {
    // not used yet
}

struct headers {
    ethernet_t   ethernet;
    ipv4_t       ipv4;
    tcp_t        tcp;
}

/*************************************************************************
*********************** P A R S E R  ***********************************
*************************************************************************/

parser MyParserBasic(packet_in packet,
                out headers hdr,
                inout metadata meta,
                inout standard_metadata_t standard_metadata) {

    state start {
        transition parse_ethernet;
    }

    state parse_ethernet {
        packet.extract(hdr.ethernet);
        transition select(hdr.ethernet.etherType) {
            TYPE_IPV4: parse_ipv4;
            default: accept;
        }
    }

    state parse_ipv4 {
        packet.extract(hdr.ipv4);
        transition accept;
    }

}

parser MyParser(packet_in packet,
                out headers hdr,
                inout metadata meta,
                inout standard_metadata_t standard_metadata) {

    state start {
        transition parse_ethernet;
    }

    state parse_ethernet {
        packet.extract(hdr.ethernet);
        transition select(hdr.ethernet.etherType) {
            TYPE_IPV4: parse_ipv4;
            default: accept;
        }
    }

    state parse_ipv4 {
        packet.extract(hdr.ipv4);
        transition select(hdr.ipv4.protocol){
            TYPE_TCP: parse_tcp;
            default: accept;
        }
    }

    state parse_tcp {
        packet.extract(hdr.tcp);
        transition accept;
    }
}

/*************************************************************************
************   C H E C K S U M    V E R I F I C A T I O N   *************
*************************************************************************/

control MyVerifyChecksum(inout headers hdr, inout metadata meta) {
    apply {  }
}


/*************************************************************************
**************  I N G R E S S   P R O C E S S I N G   *******************
*************************************************************************/

control MyIngress(inout headers hdr,
                  inout metadata meta,
                  inout standard_metadata_t standard_metadata) {

    action drop() {
        mark_to_drop(standard_metadata);
    }


    action ipv4_forward(macAddr_t dstAddr, egressSpec_t port) {
        standard_metadata.egress_spec = port;
        hdr.ethernet.srcAddr = hdr.ethernet.dstAddr;
        hdr.ethernet.dstAddr = dstAddr;
        hdr.ipv4.ttl = hdr.ipv4.ttl - 1;
    }

    table ipv4_lpm {
        key = {
            hdr.ipv4.dstAddr: lpm;
        }
        actions = {
            ipv4_forward;
            drop;
            NoAction;
        }
        size = 1024;
        default_action = drop();
    }

    apply {
        if (hdr.ipv4.isValid()) {
            ipv4_lpm.apply();
        }
    }
}

/*************************************************************************
****************  E G R E S S   P R O C E S S I N G   *******************
*************************************************************************/

control MyEgress(inout headers hdr,
                 inout metadata meta,
                 inout standard_metadata_t standard_metadata) {

    register<bit<48>>(FLOW_REGISTERS_ENTRIES) regLast1;
    register<bit<48>>(FLOW_REGISTERS_ENTRIES) regLast2;
    register<bit<48>>(FLOW_REGISTERS_ENTRIES) regFirst1;
    register<bit<48>>(FLOW_REGISTERS_ENTRIES) regFirst2;
    register<bit<32>>(FLOW_REGISTERS_ENTRIES) regSize1;
    register<bit<32>>(FLOW_REGISTERS_ENTRIES) regSize2;
    bit<32> reg_pos1; bit<32> reg_pos2;
    bit<48> val_first1; bit<48> val_first2;
    bit<48> val_last1; bit<48> val_last2;
    // For sampling, change size for number of samples.
    bit<32> val_size1; bit<32> val_size2;
    bit<32> cur_size;
    bit<48> cur_duration;
    bit<48> cur_timeout;
    register<bit<1>>(FLOW_REGISTERS_ENTRIES) isElephant1;
    register<bit<1>>(FLOW_REGISTERS_ENTRIES) isElephant2;
    bit<1> val_elephant1; bit<1> val_elephant2;

    // For random calc:
    bit<32> rresult; bit<32> lo; bit<32> hi;

    // Only for debugging
    bit<48> foo;

    action compute_hashes(ip4Addr_t ipAddr1, ip4Addr_t ipAddr2, bit<16> port1, bit<16> port2){
       //Get register position
       // Comeca em 1 para nao conflitar com o valor dos registradores que comecam em 0.
       hash(reg_pos1, HashAlgorithm.crc16, (bit<32>)1, {ipAddr1,
                                                           ipAddr2,
                                                           port1,
                                                           port2,
                                                           hdr.ipv4.protocol},
                                                           (bit<32>)FLOW_REGISTERS_ENTRIES);

       hash(reg_pos2, HashAlgorithm.crc32, (bit<32>)1, {ipAddr1,
                                                           ipAddr2,
                                                           port1,
                                                           port2,
                                                           hdr.ipv4.protocol},
                                                           (bit<32>)FLOW_REGISTERS_ENTRIES);
    }

    action mark_diffserv(bit<6> dsval) {
        hdr.ipv4.diffserv = dsval;
    }


    apply {
            // Execute only if sampled
            // lo = 0
            // hi = 1024
            // random(rresult, lo, hi)
            // if (rresult != 0)
            // skip all code below or not...

            if (hdr.tcp.isValid()){
                // Esta versao eh para i = 2.
                compute_hashes(hdr.ipv4.srcAddr, hdr.ipv4.dstAddr, hdr.tcp.srcPort, hdr.tcp.dstPort);
                // i = 1
                regLast1.read(val_last1, reg_pos1);
                cur_timeout = standard_metadata.ingress_global_timestamp - val_last1;
                if (cur_timeout > FLOW_TIMEOUT_THRESHOLD){
                    val_first1 = standard_metadata.ingress_global_timestamp;
                    regFirst1.write(reg_pos1, val_first1);
                    // change packet_length to 1, if cmss (sample)
                    val_size1 = standard_metadata.packet_length;
                    regSize1.write(reg_pos1, val_size1);
                    // Precisa dizer que nao eh elefante ???
                    isElephant1.write(reg_pos1, 0);

                }
                else {
                    regSize1.read(val_size1, reg_pos1);
                    // change packet_length to 1, if cmss (sample)
                    val_size1 = val_size1 + standard_metadata.packet_length;
                    regSize1.write(reg_pos1, val_size1);
                }
                // i = 2
                regLast2.read(val_last2, reg_pos2);
                if (cur_timeout > FLOW_TIMEOUT_THRESHOLD){
                    val_first2 = standard_metadata.ingress_global_timestamp;
                    regFirst2.write(reg_pos2, val_first2);
                    // change packet_length to 1, if cmss (sample)
                    val_size2 = standard_metadata.packet_length;
                    regSize2.write(reg_pos2, val_size2);
                    isElephant2.write(reg_pos2, 0);

                }
                else {
                    regSize2.read(val_size2, reg_pos2);
                    // change packet_length to 1, if cmss (sample)
                    val_size2 = val_size2 + standard_metadata.packet_length;
                    regSize2.write(reg_pos2, val_size2);
                }

                // update timestamp
                regLast1.write(reg_pos1, standard_metadata.ingress_global_timestamp);
                regLast2.write(reg_pos2, standard_metadata.ingress_global_timestamp);

                // Already elephant?
                isElephant1.read(val_elephant1, reg_pos1);
                isElephant2.read(val_elephant2, reg_pos2);
                if (!(val_elephant1 == 1 && val_elephant2 == 1)) {
                    // min
                    regFirst1.read(val_first1, reg_pos1);
                    regFirst2.read(val_first2, reg_pos2);
                    cur_size = (val_size1 > val_size2) ? val_size2 : val_size1;
                    cur_duration = (val_first1 > val_first2) ? val_first1 : val_first2;
                    cur_duration = standard_metadata.ingress_global_timestamp - cur_duration;
                    if (cur_size > FLOW_SIZE_THRESHOLD && cur_duration > FLOW_DURATION_THRESHOLD) {
                        // just to know in tcpdump if a flow has been reported
                        mark_diffserv(10);
                        isElephant1.write(reg_pos1, 1);
                        isElephant2.write(reg_pos2, 1);
                    }
                    else {
                        // do not reach the threshould
                        mark_diffserv(20);
                    }
                }
                else {
                    // old elephant
                    mark_diffserv(30);
                }
            }
    }
}


/*************************************************************************
*************   C H E C K S U M    C O M P U T A T I O N   **************
*************************************************************************/

control MyComputeChecksum(inout headers hdr, inout metadata meta) {
     apply {
	update_checksum(
	    hdr.ipv4.isValid(),
        { hdr.ipv4.version,
            hdr.ipv4.ihl,
            hdr.ipv4.diffserv,
            hdr.ipv4.ecn,
            hdr.ipv4.totalLen,
            hdr.ipv4.identification,
            hdr.ipv4.flags,
            hdr.ipv4.fragOffset,
            hdr.ipv4.ttl,
            hdr.ipv4.protocol,
            hdr.ipv4.srcAddr,
            hdr.ipv4.dstAddr },
            hdr.ipv4.hdrChecksum,
            HashAlgorithm.csum16);
    }
}

control MyComputeChecksumOrig(inout headers  hdr, inout metadata meta) {
     apply {
	update_checksum(
	    hdr.ipv4.isValid(),
            { hdr.ipv4.version,
	      hdr.ipv4.ihl,
              hdr.ipv4.diffserv,
              hdr.ipv4.totalLen,
              hdr.ipv4.identification,
              hdr.ipv4.flags,
              hdr.ipv4.fragOffset,
              hdr.ipv4.ttl,
              hdr.ipv4.protocol,
              hdr.ipv4.srcAddr,
              hdr.ipv4.dstAddr },
            hdr.ipv4.hdrChecksum,
            HashAlgorithm.csum16);
    }
}

/*************************************************************************
***********************  D E P A R S E R  *******************************
*************************************************************************/

control MyDeparser(packet_out packet, in headers hdr) {
    apply {
        packet.emit(hdr.ethernet);
        packet.emit(hdr.ipv4);
        packet.emit(hdr.tcp);
    }
}

/*************************************************************************
***********************  S W I T C H  *******************************
*************************************************************************/

V1Switch(
MyParser(),
MyVerifyChecksum(),
MyIngress(),
MyEgress(),
MyComputeChecksum(),
MyDeparser()
) main;
