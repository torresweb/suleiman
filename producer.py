#!/usr/bin/env python3
# Produce configurations (for simulating) and insert it in Redis

import redis
import pickle
from suleiman_lib import *

def create_setups(filename, topflow_file, tagname, repeats=1):

    tagnameStart = tagname + '_Start'
    # Para casos reais
    F_DURATION_THRESHOLD = [10]
    F_TIMEOUT_THRESHOLD = [10]
    F_SIZE_THRESHOLD = [10000000]

    MEMORIES =  [1*1024, 16*1024, 32*1024, 512*1024, 8192*1024, 65536*1024]
    F_SAMPLES_THRESHOLD = [2,3,4,8]
    DEEP = [2,5]
    SAMPLING_RATE = [256, 1024, 8192, 32768]
    VICTIM_TYPE = ['bytes']
    RESET_TYPE = ['keep']
    verbose = False

    r = redis.Redis(host='127.0.0.1', port=6379, db=0, health_check_interval=5)

    # topkeys = pd.read_csv(topflow_file, names=['flowkey']).to_dict(orient='list')['flowkey']
    topkeys = topflow_file

    # setups_core = {new_list: [] for new_list in range(NUMCORES)}
    core = 0
    # No sampling
    for memory in MEMORIES:
        for duration in F_DURATION_THRESHOLD:
            for timeout in F_TIMEOUT_THRESHOLD:
                for size in F_SIZE_THRESHOLD:
                    for deep in DEEP:
                        obj_name = tagname + 'Sk' + str(deep) + 'NoSampE' + str(memory) + 'D' + str(duration) + 'T' + str(timeout) + 'S' + str(size) + 'File' + (filename)
                        obj = ConfigProducer('ske', tagname, filename, 1, deep, obj_name, memory, timeout, duration, size, 0, 1, None, None, verbose, topkeys)
                        pick_obj = pickle.dumps(obj)
                        try:
                            r.lpush(tagname, pick_obj)
                            r.lpush(tagnameStart, obj_name)
                        except:
                            r = redis.Redis(host='127.0.0.1', port=6379, db=0, health_check_interval=5)
                            r.lpush(tagname, pick_obj)
                            r.lpush(tagnameStart, obj_name)
                        core += 1
                    for victim_type in VICTIM_TYPE:
                        for reset_type in RESET_TYPE:
                            obj_name = tagname + 'SSHNoSampE' + str(memory) + 'D' + str(duration) + 'T' + str(timeout) + 'S' + str(size) + 'C' + str(victim_type) + 'R' + str(reset_type) + 'File' + (filename)
                            obj = ConfigProducer('ssh', tagname, filename, 1, None, obj_name, memory, timeout, duration, size, 0, 1, victim_type, reset_type, verbose, topkeys)
                            pick_obj = pickle.dumps(obj)
                            try:
                                r.lpush(tagname, pick_obj)
                                r.lpush(tagnameStart, obj_name)
                            except:
                                r = redis.Redis(host='127.0.0.1', port=6379, db=0, health_check_interval=5)
                                r.lpush(tagname, pick_obj)
                                r.lpush(tagnameStart, obj_name)
                            core += 1

    # Sampling
    for memory in MEMORIES:
        for duration in F_DURATION_THRESHOLD:
            for timeout in F_TIMEOUT_THRESHOLD:
                for size in F_SIZE_THRESHOLD:
                    for samples in F_SAMPLES_THRESHOLD:
                        for srate in SAMPLING_RATE:
                            for deep in DEEP:
                                obj_name = tagname + 'Sk' + str(deep) + 'Samp' + str(srate) + 's' + str(samples) + 'E' + str(memory) + 'D' + str(duration) + 'T' + str(timeout) + 'S' + str(size) + 'File' + (filename)
                                obj = ConfigProducer('ske', tagname, filename, repeats, deep, obj_name, memory, timeout, duration, size, samples, srate, None, None, verbose, topkeys)
                                pick_obj = pickle.dumps(obj)
                                try:
                                    r.lpush(tagname, pick_obj)
                                    r.lpush(tagnameStart, obj_name)
                                except:
                                    r = redis.Redis(host='127.0.0.1', port=6379, db=0, health_check_interval=5)
                                    r.lpush(tagname, pick_obj)
                                    r.lpush(tagnameStart, obj_name)
                                core += 1
                            for victim_type in VICTIM_TYPE:
                                for reset_type in RESET_TYPE:
                                    obj_name = tagname + 'SSHSamp' + str(srate) + 's' + str(samples) + 'E' + str(memory) + 'D' + str(duration) + 'T' + str(timeout) + 'S' + str(size) + 'C' + str(victim_type) + 'R' + str(reset_type) + 'File' + (filename)
                                    obj = ConfigProducer('ssh', tagname, filename, repeats, None, obj_name, memory, timeout, duration, size, samples, srate, victim_type, reset_type, verbose, topkeys)
                                    pick_obj = pickle.dumps(obj)
                                    try:
                                        r.lpush(tagname, pick_obj)
                                        r.lpush(tagnameStart, obj_name)
                                    except:
                                        r = redis.Redis(host='127.0.0.1', port=6379, db=0, health_check_interval=5)
                                        r.lpush(tagname, pick_obj)
                                        r.lpush(tagnameStart, obj_name)
                                    core += 1

    print ('Total setups: %d' % (core))

if __name__ == '__main__':

    repeats = 10


    filename = 'caidaDirA.pcap'
    # Elephant flows file:
    topflow = 'topflows_caidaDirA.pcap.csv'
    tagname = 'FINAL'
    create_setups(filename, topflow, tagname, repeats)

    filename = 'caidaDirB.pcap'
    # Elephant flows file:
    topflow = 'topflows_caidaDirB.pcap.csv'
    tagname = 'FINAL'
    create_setups(filename, topflow, tagname, repeats)


    filename = 'rnp5m10.pcap'
    # Elephant flows file:
    topflow = 'topflows_rnp5m10.pcap.csv'
    tagname = 'FINAL'
    create_setups(filename, topflow, tagname, repeats)
